import { Component, Inject } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialoagComponent } from "../app/dialoag/dialoag.component";
import { all } from 'q';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  raw = 2;
  column = 2;
  array = [];

  alert: boolean;
  turn: boolean = true;

  constructor(public dialog: MatDialog) {
    for (let i = 0; i <= this.raw; i++) {
      let temp = [];
      for (let j = 0; j <= this.column; j++) {
        temp.push({ s: "", raw: i, column: j });
      }
      this.array.push(temp);
    }
  }

  openModal(ch) {
    let message = "";
    if (!ch) {
      message = 'Game Draw ...';
    } else {
      message = ch + ' Win the game ...';
    }
    let dialog = this.dialog.open(DialoagComponent, {
      width: '500px',
      data: { name: message }
    });
  }

  change(r, c) {
    if (this.array[r][c].s == "") {
      this.array[r][c].s = "X";
      this.win2(r, c);
    }
    this.ai_turn();
  }

  ai_turn() {
    let flag: boolean = false;
    let all_filled = true;
    for (let i = 0; i <= this.raw; i++) {
      for (let j = 0; j <= this.column; j++) {
        if (this.array[i][j].s == "" && !flag) {
          flag = true;
          this.array[i][j].s = "O"
          this.win2(i, j);
        }
        if (this.array[i][j].s == "" ){
          all_filled = false; 
          // console.log("asd");
          // grab all empty postion 
          // pass to defend 
          // pass it attack
        }
      }
    }
    if (all_filled == true){
      this.openModal(null);
    }
  }

  is_X_winning() {

  }

  try_to_win(){

  }
 
  check_diagonal(f,ch,r,c) {
    if(f == true){
      if (r!= -1 && c!= -1){
        if (this.array[r][c].s == ch) {
          return this.check_diagonal(true, ch, r-1, c-1);
        } else {
          return false;
        }
      } else {
        return true;
      }
    } else{
      return false;
    }
  }

  check_reverse_diagonal(f,ch,r,c) {
    // console.log(this.array[r][c]);
    if(f == true){
      if (r != -1 && c != this.column - 1 ){
        if (this.array[r][c].s == ch) {
          return this.check_reverse_diagonal(true, ch, r-1, c+1);
        } else {
          return false;
        }
      } else {
        return true;
      }
    } else{
      return false;
    }
  }

  checkStraitLines(ch, flag) {
    for (let i = 0; i <= this.raw; i++) {
      if (this.array[i][0].s == "X" && this.array[i][1].s == "X" && this.array[i][2].s == "X") {
        this.openModal("X");
      }
      else {
        if (this.array[i][0].s == "O" && this.array[i][1].s == "O" && this.array[i][2].s == "O") {
          this.openModal("O");
        }
      }
    }

    if(this.check_diagonal(true,"X",this.raw,this.column) == true){
      this.openModal("X");
    }
    if(this.check_diagonal(true,"O",this.raw,this.column) == true){
      this.openModal("O");
    }
    if(this.check_reverse_diagonal(true, "X", this.raw, 0) == true){
      this.openModal("X");
    }
    if(this.check_reverse_diagonal(true, "O", this.raw, 0) == true){
      console.log("dakho");
      this.openModal("O");
    }

    for (let i = 0; i <= this.column; i++) {
      if (this.array[2][i].s == "X" && this.array[1][i].s == "X" && this.array[0][i].s == "X") {
        this.openModal("X");
      }
      else {
        if (this.array[2][i].s == "O" && this.array[1][i].s == "O" && this.array[0][i].s == "O") {
          this.openModal("O");
        }
      }
    }
  }

  win2(r, c) {
    this.checkStraitLines("X", true);
    this.checkStraitLines("O", false);
  }

  reset() {
    window.location.reload();
  }

}