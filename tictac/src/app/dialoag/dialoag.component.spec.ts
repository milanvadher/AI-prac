import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialoagComponent } from './dialoag.component';

describe('DialoagComponent', () => {
  let component: DialoagComponent;
  let fixture: ComponentFixture<DialoagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialoagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialoagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
