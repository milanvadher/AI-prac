import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialoag',
  templateUrl: './dialoag.component.html',
  styleUrls: ['./dialoag.component.css']
})
export class DialoagComponent implements OnInit {

  constructor(private matDialogRef: MatDialogRef<DialoagComponent>, @Inject(MAT_DIALOG_DATA) public data: string) { }

  ngOnInit() {
  }

  newGame() {
    window.location.reload();
    this.matDialogRef.close();
  }

}
