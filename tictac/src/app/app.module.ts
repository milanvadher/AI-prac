import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import 'hammerjs';

import { AppComponent } from './app.component';
import { DialoagComponent } from './dialoag/dialoag.component';


@NgModule({
  declarations: [
    AppComponent,
    DialoagComponent    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule, 
    MatButtonModule,
    MatDialogModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [DialoagComponent]
})
export class AppModule { }
